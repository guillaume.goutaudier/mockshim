package main

import (
    "fmt"
    "testing"
    "github.com/hyperledger/fabric/core/chaincode/shim"
)

func TestInvokeGet(t *testing.T) {
    fmt.Println("Entering TestInvokeGet")

    // First we create a Stub for the ChainCode
    stub := shim.NewMockStub("mockStub", new(SimpleAsset))
    if stub == nil {
        t.Fatalf("MockStub creation failed")
    }

    // We first initialize the ChainCode and test that the key "a" has been assigned a value 
    init_result := stub.MockInit("1", [][]byte{})
    if init_result.Status != shim.OK {
        t.Fatalf("Failed to initialize ChainCode")
    }
    fmt.Println("Chaincode successfully initialized")
    valAsbytes, err := stub.GetState("a")
    if err != nil {
        t.Errorf("Failed to get state for key a")
    } else if valAsbytes == nil {
        t.Errorf("No value for key a")
    }
    fmt.Println("Value for key a: "+string(valAsbytes))

    // Then we validate that we can invoke the chaincode to assign a value to key "b"
    invoke_result := stub.MockInvoke("2", [][]byte{[]byte("set"),[]byte("b"),[]byte("20")})
    if invoke_result.Status != shim.OK {
        t.Fatalf("Failed to invoke ChainCode")
    }
    valAsbytes, err = stub.GetState("b")
    if err != nil {
        t.Errorf("Failed to get state for key b")
    } else if valAsbytes == nil {
        t.Errorf("No value for key b")
    } else if string(valAsbytes) != "20" {
        t. Errorf("Value b is not equal to 20")
    }
    fmt.Println("Value for key b: "+string(valAsbytes))
}

