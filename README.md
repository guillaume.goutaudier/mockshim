# Introduction
This project is just a very basic set of code snippets and commands that I captured while playing with the Hyperledger Fabric mockshim library. It can actually be used as a starting point to setup a Go ChainCode development environment.

# Pre-requisites
You will need a working Go installation. 
There are various tutorials on how to install Go.
In my case, I just used Brew on my Mac:
```
$ brew install go
```

# Validate the Go environment is working well
Before getting into the complexities of Chain Code development, let's first check that we have a working Go installation.

Let's build the test application located in the `src/test` directory. The application is in the `test.go`file:

```
package main

import "fmt"

func main() {
   fmt.Println("Hello, World!")
}
```

To instrusct Go where to find our various applications, we need to set the GOPATH to match our project directory, i.e.:
```
$ export GOPATH=/Users/goutaudi/mockshim
```

Note that the go compiler also looks for .go files in the working directory, so as an alternative you have gone directly in `src/test` directory.

To build and run the test application:
```
$ go build test
$ ./test
Hello, World!
```

Notice here that Go knew where to find the source code (src/test/test.go). This is just a Go convention that we will reuse for the Chain Code development.


# Build a basic chaincode
The chaincode we are using is the `sacc` chaincode from the official Hyperledger Fabric documentation (with a few simplifications). This is a very simple and convenient chaincode that manages key value pairs on the ledger. 

First compile the chaincode. As we have dependencies on the hyperledger libraries (not only the standard one), we also need to download them:
```
$ go get -d ./...
$ go build sacc
```

We can not run the sacc executable directly as it is supposed to run on Hyperledger peers only. To be able to test it locally, we are going to use the MockShim library. 



# Use the Mock shim library to test the chaincode
We are leveraging the standard "testing" go library here. This is why the test file needs to be 
called `sacc_test.go`. The MockShim library is part of the Shim library, so there is no need to import any additional library. 

You can have a look at the `sacc_test.go` file. The most important functions are:
- `NewMockStub()` to initialize a shim object (on the actual Blockchain this is done with the shim.Start() function)
- `MockInit()` to initialize the ChainCode, just as it would be initialized on the Blockchain
- `MockInvoke()` for invoking all the ChainCode functions. Note here that this function can be called without calling `MockInit()` first. 

To test this code go to the `src/sacc` directory and simply run:
```
$ go test
Entering TestInvokeGet
Chaincode successfully initialized
Value for key a: 10
Value for key b: 20
PASS
ok  	sacc	0.035s
```

You should see a similar output, indicating that all the tests have been executed successfully. As a side note, it is actually not mandatory to compile the chaincode before running the tests. So you could have run the test directly. 

That's all. Happy coding!





